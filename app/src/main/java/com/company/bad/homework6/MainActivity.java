package com.company.bad.homework6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView textView = findViewById(R.id.textView);

        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                MyObservable myObservable = new MyObservable();
                myObservable
                        .from(new Callable() {
                            @Override
                            public String execute() {
                                MyLogger.debug(Thread.currentThread().getName() + " " + Thread.currentThread().getId() + " Callable ");
                                try {
                                    TimeUnit.SECONDS.sleep(4);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                return "200";
                            }
                        })
                        .observeOn(getMainLooper())
                        .subscribeOn(getMainLooper())
                        .subscribe(new Callback() {
                            @Override
                            public void getResponse(String response) {
                                textView.setText(response);
                                MyLogger.debug(Thread.currentThread().getName() + " " + Thread.currentThread().getId() + " Callback ");
                            }
                        });
            }
        });
        myThread.start();



//        MyObservable myObservable = new MyObservable();
//        myObservable
//                .from(new Callable() {
//                    @Override
//                    public String execute() {
//                        MyLogger.debug(Thread.currentThread().getName() + " " + Thread.currentThread().getId() + " Callable ");
//                        try {
//                            TimeUnit.SECONDS.sleep(4);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        return "200";
//                    }
//                })
//                .observeOn(getMainLooper())
//                .subscribeOn(getMainLooper())
//                .subscribe(new Callback() {
//                    @Override
//                    public void getResponse(String response) {
//                        textView.setText(response);
//                        MyLogger.debug(Thread.currentThread().getName() + " " + Thread.currentThread().getId() + " Callback ");
//                    }
//                });
    }
}
