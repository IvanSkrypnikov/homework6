package com.company.bad.homework6;

import android.os.Looper;

public class MyObservable {

    private Callable callable = null;
    private Looper looper = null;
    private Looper mainLooper = null;
    private Callback callback = null;

    public MyObservable from(Callable callable){
        this.callable = callable;
        return this;
    }

    public MyObservable subscribeOn(Looper looper){
        this.looper = looper;
        return this;
    }

    public MyObservable observeOn(Looper mainLooper){
        this.mainLooper = mainLooper;
        return this;
    }

    public MyObservable subscribe(Callback callback){
        this.callback = callback;
        if (mainLooper == null)
            mainLooper = looper;

        MyWorkerThread myWorkerThread = new MyWorkerThread(looper,mainLooper,callable,callback);
        myWorkerThread.run();

        return this;
    }

}
