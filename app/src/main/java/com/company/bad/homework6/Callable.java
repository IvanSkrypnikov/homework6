package com.company.bad.homework6;

import android.os.Message;

public abstract class Callable {
    public abstract String execute();
}
