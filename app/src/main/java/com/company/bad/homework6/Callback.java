package com.company.bad.homework6;

public abstract class Callback {
    public abstract void getResponse(String response);
}
