package com.company.bad.homework6;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.concurrent.TimeUnit;

public class MyWorkerThread extends Thread{
    private Handler workerHandler;
    private Handler responseHandler;
    private Callback callback;
    private Callable callable;
    private Looper looper;
    private Looper mainLooper;

    public MyWorkerThread(Looper looper, Looper mainLooper, Callable callable, Callback callback) {
        this.callable = callable;
        this.callback = callback;
        this.looper = looper;
        this.mainLooper = mainLooper;
    }

    @Override
    public void run() {
       if (looper == null){
            Looper.prepare();
            prepareHandler();
            Looper.loop();
       }
        prepareHandler();
    }

    public void prepareHandler() {
        responseHandler = new Handler(mainLooper==null?Looper.myLooper():mainLooper, new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                MyLogger.debug(Thread.currentThread().getName() + " " + Thread.currentThread().getId() + " responseHandler ");
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                String result = null;
                if (msg.what == 1)
                    result = (String) msg.obj;
                if (result == null)
                    return false;
                callback.getResponse(result);
                return true;
            }
        });
        workerHandler = new Handler(looper==null?Looper.myLooper():looper);
        workerHandler.post(new Runnable() {
            @Override
            public void run() {
                MyLogger.debug(Thread.currentThread().getName() + " " + Thread.currentThread().getId() + " workerHandler ");
                String result = callable.execute();
                Message message = workerHandler.obtainMessage(1, result);
                responseHandler.sendMessage(message);
            }
        });
    }
}
