package com.company.bad.homework6;

import android.util.Log;

public class MyLogger {

    public static final boolean isDebug = true;
    public static final String TAG = "MyLogger";

    public static void debug(String statement){
        if (isDebug) {
            Log.d(TAG, statement);
        }
    }
}
